#ifndef UTILS_INTERRUPT_INTERRUPT_H
#define UTILS_INTERRUPT_INTERRUPT_H

#include "../compiler.h"
#include "../parts.h"

#if defined(__DOXYGEN__)
#  define ISR(vect)
#elif defined(__GNUC__)
#  include <avr/interrupt.h>
#elif defined(__ICCAVR__)
#  define __ISR(x) _Pragma(#x)
#  define ISR(vect) __ISR(vector=vect) __interrupt void handler_##vect(void)
#endif

#if XMEGA

#define irq_initialize_vectors() \
    PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
#endif

#ifdef __GNUC__
#  define cpu_irq_enable()     sei()
#  define cpu_irq_disable()    cli()
#else
#  define cpu_irq_enable()     __enable_interrupt()
#  define cpu_irq_disable()    __disable_interrupt()
#endif

typedef uint8_t irqflags_t;

static inline irqflags_t cpu_irq_save(void)
{
    irqflags_t flags = SREG;
    cpu_irq_disable();
    return flags;
}

static inline void cpu_irq_restore(irqflags_t flags)
{
    barrier();
    SREG = flags;
}

static inline bool cpu_irq_is_enabled_flags(irqflags_t flags)
{
#if XMEGA
#  ifdef __GNUC__
    return flags & CPU_I_bm;
#  else
    return flags & I_bm;
#  endif
#elif MEGA
    return flags & (1 << SREG_I);
#endif
}

#define cpu_irq_is_enabled()             cpu_irq_is_enabled_flags(SREG)


// Deprecated definitions.
#define Enable_global_interrupt()        cpu_irq_enable()
#define Disable_global_interrupt()       cpu_irq_disable()
#define Is_global_interrupt_enabled()    cpu_irq_is_enabled()


#endif /* UTILS_INTERRUPT_INTERRUPT_H */
