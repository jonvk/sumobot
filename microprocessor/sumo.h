//Declaration of our functions
void USART_init(void);
void PIN_init(void);

char USART_receive(void);
void USART_send(char data);
void USART_putstring(char* StringPtr);
