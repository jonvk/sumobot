#ifndef UTILS_COMPILER_H
#define UTILS_COMPILER_H

#if defined(__GNUC__)
#   include <avr/io.h>
#elif defined(__ICCAVR__)
#   include <ioavr.h>
#   include <intrinsics.h>
#else
#   error "Unsupported compiler."
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>

#ifdef __ICCAVR__


#define __asm__              asm
#define __inline__           inline
#define __volatile__

#endif

#ifdef __GNUC__
#  define barrier()        asm volatile("" ::: "memory")
#else
#  define barrier()        asm ("")
#endif

//_____ M A C R O S ________________________________________________________


#if (defined __GNUC__)
#   define __always_inline   inline __attribute__((__always_inline__))
#elif (defined __ICCAVR__)
#   define __always_inline   _Pragma("inline=forced")
#endif


#if defined(_ASSERT_ENABLE_)
#  if defined(TEST_SUITE_DEFINE_ASSERT_MACRO)
    // Assert() is defined in unit_test/suite.h
#    include "unit_test/suite.h"
#  else
#    define Assert(expr) \
    {\
        if (!(expr)) while (true);\
    }
#  endif
#else
#  define Assert(expr) ((void) 0)
#endif



#define MSB(u16)             (((uint8_t* )&u16)[1])
#define LSB(u16)             (((uint8_t* )&u16)[0])


#include "interrupt.h"

#endif  // UTILS_COMPILER_H
