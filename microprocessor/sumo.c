// _ASSERT_ENABLE_ is used for enabling assert, typical for debug purposes
#define _ASSERT_ENABLE_
#include <string.h>

// set the correct BAUD and F_CPU defines before including setbaud.h
#include "conf_clock.h"
#include "conf_uart.h"

#include <util/delay.h>
#include "compiler.h"


#include <util/setbaud.h>
#include <avr/interrupt.h>

#define LDPIN 6;
#define RDPIN 7;
#define LSPIN 0;
#define RSPIN 0;

uint8_t send_char;
uint8_t read_char;
uint8_t char_waiting;

uint8_t left_speed;
uint8_t right_speed;
uint8_t left_dir;
uint8_t right_dir;

ISR(UART0_DATA_EMPTY_IRQ)
{
  UDR0 = send_char;
  // no more data to send, turn off data ready interrupt
  UCSR0B &= ~(1 << UDRIE0);
}

ISR(UART0_RX_IRQ)
{
  read_char = UDR0;
  char_waiting = true;
}

static void uart_init(void)
{
#if defined UBRR0H
  // get the values from the setbaud tool
  UBRR0H = UBRRH_VALUE;
  UBRR0L = UBRRL_VALUE;
#else
#error "Device is not supported by the driver"
#endif

#if USE_2X
  UCSR0A |= (1 << U2X0);
#endif

  // enable RX and TX and set interrupts on rx complete
  UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0);

  // 8-bit, 1 stop bit, no parity, asynchronous UART
  UCSR0C = (1 << UCSZ01) | (1 << UCSZ00) | (0 << USBS0) |
    (0 << UPM01) | (0 << UPM00) | (0 << UMSEL01) |
    (0 << UMSEL00);
  char_waiting = false;
}

/**
 * Set the Waveform generation mode using WGM bits and Compare output
 * mode using COM bits
 */
static void pwm_init() {
    DDRB |= (1 << DDB1)|(1 << DDB2);
    // PB1 and PB2 is now an output

    ICR1 = 0xFF;
    // set TOP to 16bit

    TCCR1A |= (1 << COM1A1)|(1 << COM1B1);
    // set none-inverting mode

    TCCR1A |= (1 << WGM11);
    TCCR1B |= (1 << WGM12)|(1 << WGM13);
    // set Fast PWM mode using ICR1 as TOP
    
    TCCR1B |= (1 << CS10);
    // START the timer with no prescaler}
}

static inline void uart_putchar(uint8_t data)
{
  // Disable interrupts to get exclusive access to ring_buffer_out.
  cli();
  send_char = data;
  // First data in buffer, enable data ready interrupt
  UCSR0B |=  (1 << UDRIE0);
  // Re-enable interrupts
  sei();
}

static inline uint8_t uart_getchar(void)
{
  uint8_t data;
  // Disable interrupts to get exclusive access to ring_buffer_out.
  cli();
  data = read_char;
  char_waiting = false;

  // Re-enable interrupts
  sei();
  return data;
}


static inline bool uart_char_waiting(void)
{
  return char_waiting;
}

int main(void)
{
  // Left speed, direction, right speed, direction
  uint8_t ls, ld, rs, rd;
  uint8_t tmp;
  uint8_t data;

  pwm_init();
  cli();
  uart_init();
  sei();


  DDRB |= (1 << 6) | (1 << 7);
  PORTB |= (1 << 6) | (1 << 7);
  OCR1A = 0;
  OCR1B = 0;
  uart_putchar('O'); _delay_ms(2); uart_putchar('K');
  // Check if we have received the string we sent
  do {
    // Wait for next character
    while (!uart_char_waiting()) { _delay_ms(2);}
    OCR1A = OCR1B = 0xFF;
    data = uart_getchar();
    ld = ((data>>7) & 1);
    ls = ((data>>4) & 7) * 18;
    rd = ((data>>3) & 1);
    rs = (data & 7) * 18;
    tmp = ld << LDPIN;
    tmp |= rd << RDPIN;
    PORTB = tmp;
    // set PWM
    OCR1A = ls;
    OCR1B = rs;
    uart_putchar('O'); _delay_ms(2); uart_putchar('K'); _delay_ms(2); uart_putchar(' '); _delay_ms(2); uart_putchar(data);
  } while (true);
}
