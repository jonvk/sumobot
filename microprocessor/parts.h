#ifndef MEGA_PARTS_H
#define MEGA_PARTS_H

#define part_is_defined(part) \
    (defined(__ ## part ## __) || defined(__AVR_ ## part ## __))

// ATmegaxx0
#define MEGA_XX0 (\
        part_is_defined(ATmega640) || \
        part_is_defined(ATmega1280) || \
        part_is_defined(ATmega2560) \
    )

// ATmegxx1
#define MEGA_XX1 (\
        part_is_defined(ATmega1281) || \
        part_is_defined(ATmega2561) \
    )

// ATmegaxx0/xx1
#define MEGA_XX0_1  (MEGA_XX0 || MEGA_XX1)


// ATmegaxx4
#define MEGA_XX4 (\
        part_is_defined(ATmega164A) || part_is_defined(ATmega164PA) || \
        part_is_defined(ATmega324) || part_is_defined(ATmega324A) || \
        part_is_defined(ATmega324PA) || part_is_defined(ATmega644) || \
        part_is_defined(ATmega644A) || part_is_defined(ATmega644PA) || \
        part_is_defined(ATmega1284P) \
    )

// ATmegaxx4
#define MEGA_XX4_A (\
        part_is_defined(ATmega164A) || part_is_defined(ATmega164PA) || \
        part_is_defined(ATmega324A) || part_is_defined(ATmega324PA) || \
        part_is_defined(ATmega644A) || part_is_defined(ATmega644PA) || \
        part_is_defined(ATmega1284P) \
    )

// ATmegaxx8
#define MEGA_XX8 (\
        part_is_defined(ATmega48) || part_is_defined(ATmega48A) || \
        part_is_defined(ATmega48PA) || part_is_defined(ATmega88) || \
        part_is_defined(ATmega88A) || part_is_defined(ATmega88PA) || \
        part_is_defined(ATmega168) || part_is_defined(ATmega168A) || \
        part_is_defined(ATmega168PA) || part_is_defined(ATmega328) || \
        part_is_defined(ATmega328P) \
    )

// ATmegaxx8A/P/PA
#define MEGA_XX8_A (\
        part_is_defined(ATmega48A) || part_is_defined(ATmega48PA) || \
        part_is_defined(ATmega88A) || part_is_defined(ATmega88PA) || \
        part_is_defined(ATmega168A) || part_is_defined(ATmega168PA) || \
        part_is_defined(ATmega328P) \
    )

// ATmegaxx
#define MEGA_XX (\
        part_is_defined(ATmega16) || part_is_defined(ATmega16A) || \
        part_is_defined(ATmega32) || part_is_defined(ATmega32A) || \
        part_is_defined(ATmega64) || part_is_defined(ATmega64A) || \
        part_is_defined(ATmega128) || part_is_defined(ATmega128A) \
    )

// ATmegaxxA/P/PA
#define MEGA_XX_A (\
        part_is_defined(ATmega16A) || part_is_defined(ATmega32A) || \
        part_is_defined(ATmega64A) || part_is_defined(ATmega128A) \
    )


// Unspecified
#define MEGA_UNSPECIFIED (\
        part_is_defined(ATmega16) || part_is_defined(ATmega16A) || \
        part_is_defined(ATmega32) || part_is_defined(ATmega32A) || \
        part_is_defined(ATmega64) || part_is_defined(ATmega64A) || \
        part_is_defined(ATmega128) || part_is_defined(ATmega128A) || \
        part_is_defined(ATmega169P) || part_is_defined(ATmega169PA) || \
        part_is_defined(ATmega329P) || part_is_defined(ATmega329PA) \
    )

// All megaAVR devices
#define MEGA (MEGA_XX0_1 || MEGA_XX4 || MEGA_XX8 || MEGA_XX || MEGA_UNSPECIFIED)

#endif /* MEGA_PARTS_H */
