#!/usr/bin/python

PORT="/dev/rfcomm0"

import serial
import time
from getch import getch
from control import Controller
controller = Controller()
#rfcomm0
s = serial.Serial(PORT, 9600, timeout=0.01)

while True:
    c = getch()
    v = controller.key(c)
    s.write(bytes([v]))
    print(s.read(4))
    print(v)
    print( ((v >> 7) & 1),\
           ((v>>4) & 7) * 36, \
           ((v>>3) & 1), \
           (v & 7)*36)
