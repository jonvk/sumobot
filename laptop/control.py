MAX_SPEED = 7

class Controller:
    def __init__(self):
        self.keys = {
            'w': self.forward,
            's': self.back,
            'd': self.right,
            'a': self.left,
            'e': self.right2,
            'q': self.left2,
            'x': exit,
        }

        self.state = {
            'ls': 0,
            'ld': 1,
            'rs': 0,
            'rd': 1,
        }

    def value(self):
        """ Return the byte to signal state """
        return \
            (self.state['ld'] & 1)<<7 | \
            (self.state['ls'] & 7 )<<4 | \
            (self.state['rd'] & 1)<<3 | \
            (self.state['rs'] & 7)

    def key(self, k):
        """ Do action and return state byte """
        if k in self.keys:
            self.keys[k]()
        else:
            print('error', k)
        return self.value()


    def forward(self):
        state = self.state
        if state['ld'] == state['rd']:
            # Already moving forward
            if state['ld']:
                s = min(state['ls']+1, MAX_SPEED)
                state['ls'] = s
                state['rs'] = s
                return

        state['ls'] = 0
        state['rs'] = 0
        state['ld'] = 1
        state['rd'] = 1

    def back(self):
        state = self.state
        if state['ld'] == state['rd']:
            # Already moving backwards
            if not state['ld']:
                s = min(state['ls']+1, MAX_SPEED)
                state['ls'] = s
                state['rs'] = s
                return

        state['ls'] = 0
        state['rs'] = 0
        state['ld'] = 0
        state['rd'] = 0
        
    def left(self):
        """ Turn on spot """
        state = self.state
        if state['rd'] and (not state['ld']) and state['ls']:
            # Already turning left
            state['ls'] = min(state['ls']+1, MAX_SPEED)
            state['rs'] = min(state['rs']+1, MAX_SPEED)

        else:
            state['ls'] = 1
            state['rs'] = 1
            state['ld'] = 0
            state['rd'] = 1

    def right(self):
        """ Turn on spot """
        state = self.state
        if state['ld'] and (not state['rd']) and state['rs']:
            # Already turning left
            state['rs'] = min(state['rs']+1, MAX_SPEED)
            state['ls'] = min(state['ls']+1, MAX_SPEED)

        else:
            state['ls'] = 1
            state['rs'] = 1
            state['ld'] = 1
            state['rd'] = 0

    def left2(self):
        """ Turn around wheel """
        state = self.state
        if state['rs'] and not state['ls']:
            state['rs'] = min(state['rs']+1, MAX_SPEED)
        else:
            state['rs'] = 1
            state['ls'] = 0
            state['rd'] = 1
            state['ld'] = 0

    def right2(self):
        """ Turn around wheel """
        state = self.state
        if state['ls'] and not state['rs']:
            state['ls'] = min(state['ls']+1, MAX_SPEED)
        else:
            state['rs'] = 0
            state['ls'] = 1
            state['rd'] = 0
            state['ld'] = 1
